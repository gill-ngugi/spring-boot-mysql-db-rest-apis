package com.example.mysqljparestfulapis.controllers;

import com.example.mysqljparestfulapis.models.Continent;
import com.example.mysqljparestfulapis.models.Country;
import com.example.mysqljparestfulapis.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CountryController {
    @Autowired
    private CountryService countryService;

    // Get all countries
    @GetMapping("/countries")
    public List<Country> getCountries() {
        return countryService.getAllCountries();
    }

    // Get all countries by continent
    @GetMapping("/countries/{continent}")
    public List<Country> getCountriesByContinent(@PathVariable Continent continent) {
        return countryService.getCountriesByContinent(continent);
    }

    // Get one country by ID
    @GetMapping("/country/{id}")
    public Country getCountry(@PathVariable Long id) {
        return countryService.getCountry(id);
    }

    // Get one country by name
    @GetMapping("/countryByName/{name}")
    public Country getCountryByName(@PathVariable String name) {
        return countryService.getCountryByName(name);
    }

    // Post one country
    @PostMapping("/country")
    public Country postCountry(@RequestBody Country country) {
        return countryService.postCountry(country);
    }

    // Post multiple country
    @PostMapping("/countries")
    public List<Country> postCountries(@RequestBody List<Country> countries) {
        return countryService.postCountries(countries);
    }

    // Update a country
    @PutMapping("/country")
    public Country updateCountry(@RequestBody Country country) {
        return countryService.updateCountry(country);
    }

    // Delete a country
    @DeleteMapping("/country/{id}")
    public String deleteCountry(@PathVariable Long id) {
        countryService.deleteCountry(id);
        return "Country with ID " + id + " deleted!";
    }
}
