package com.example.mysqljparestfulapis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MysqlJpaRestfulApisApplication {

	public static void main(String[] args) {
		SpringApplication.run(MysqlJpaRestfulApisApplication.class, args);
	}

}
