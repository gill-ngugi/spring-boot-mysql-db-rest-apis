package com.example.mysqljparestfulapis.services;

import com.example.mysqljparestfulapis.models.Continent;
import com.example.mysqljparestfulapis.models.Country;
import com.example.mysqljparestfulapis.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    // Get all countries
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    // Get all countries by continent
    public List<Country> getCountriesByContinent(Continent continent) {
        return countryRepository.findCountriesByContinent(continent).orElse(null);
    }

    // Get one country by ID
    public Country getCountry(Long id) {
        return countryRepository.findById(id).orElse(null);
    }

    // Get one country by name
    public Country getCountryByName(String name) {
        return countryRepository.findCountryByName(name).orElse(null);
    }

    // Post one country
    public Country postCountry(Country country) {
        return countryRepository.save(country);
    }

    // Post multiple countries
    public List<Country> postCountries(List<Country> countries) {
        return countryRepository.saveAll(countries);
    }

    // Update a country
    public Country updateCountry(Country country) {
        Country existingCountry = countryRepository.findById(country.getId()).orElse(null);
        existingCountry.setName(country.getName());
        existingCountry.setLeader(country.getLeader());
        existingCountry.setContinent(country.getContinent());
        existingCountry.setCities(country.getCities());
        return countryRepository.save(existingCountry);
    }

    // Delete a country
    public String deleteCountry(Long id) {
        countryRepository.deleteById(id);
        return "Country with ID " + id + "deleted";
    }

}
