package com.example.mysqljparestfulapis.repository;

import com.example.mysqljparestfulapis.models.Continent;
import com.example.mysqljparestfulapis.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CountryRepository extends JpaRepository<Country, Long> {
    Optional<List<Country>> findCountriesByContinent(Continent continent);
    Optional<Country> findCountryByName(String name);
}
