package com.example.mysqljparestfulapis.models;

public enum Continent {
    Africa, Australia, Europe, North_America, South_America, Asia, Antarctica
}
