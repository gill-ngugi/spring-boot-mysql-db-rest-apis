package com.example.mysqljparestfulapis.models;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@NoArgsConstructor
@Entity
@Table(name = "cities")
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private BigInteger population;
    @ElementCollection(targetClass = String.class)
    private List<String> places;
//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "country_id", referencedColumnName = "id")
//    private Country country;

    public City(String name, BigInteger population, List<String> places) {
        this.name = name;
        this.population = population;
        this.places = places;
//        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigInteger getPopulation() {
        return population;
    }

    public void setPopulation(BigInteger population) {
        this.population = population;
    }

    public List<String> getPlaces() {
        return places;
    }

    public void setPlaces(List<String> places) {
        this.places = places;
    }

//    public Country getCountry() {
//        return country;
//    }
//
//    public void setCountry(Country country) {
//        this.country = country;
//    }
}
