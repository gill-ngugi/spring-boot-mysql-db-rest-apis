package com.example.mysqljparestfulapis.models;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Entity
@Table(name = "countries")
public class Country {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private Continent continent;
    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "leader_id", referencedColumnName = "id")
    private Leader leader;
    @OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name = "city_id", referencedColumnName = "id")
    private List<City> cities;

    public Country(String name, Continent continent, Leader leader, List<City> cities) {
        this.name = name;
        this.continent = continent;
        this.leader = leader;
        this.cities = cities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    public Leader getLeader() {
        return leader;
    }

    public void setLeader(Leader leader) {
        this.leader = leader;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
