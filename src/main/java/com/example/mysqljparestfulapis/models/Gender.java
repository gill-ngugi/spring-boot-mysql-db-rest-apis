package com.example.mysqljparestfulapis.models;

public enum Gender {
    MALE, FEMALE
}
