# STEPS

!!!RED!!! Make sure you add this dependency to pom.xml
 =>	<!-- https://mvnrepository.com/artifact/org.javassist/javassist -->
   		<dependency>
   			<groupId>org.javassist</groupId>
   			<artifactId>javassist</artifactId>
   			<version>3.25.0-GA</version>
   		</dependency>
   	</dependencies>
   	
   	=> Then run the project from the main class